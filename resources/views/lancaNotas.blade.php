@extends('template')
@section('conteudo')
    <h4> Lançamento de Notas </h4>

    <table class="table table-striped table-bordered">
        <tr>
            <th>Nome</th>
            <th>Turma</th>
            <th>nota 1</th>
            <th>nota 2</th>
            <th>nota 3</th>
        </tr>

        @foreach ($alunos as $aluno)
            <tr>  
                <td><a href="{{ url('listaDados/'.$aluno->id)}}"> {{ $aluno->nome }} </a> </td>
                <td>{{ $aluno->turma }}</td>   
                @php $aux_nota = false; @endphp
                @foreach ($notas as $nota)
                    @if ($aluno->id == $nota->id_aluno)
                        <td>{{ $nota->nota_1 }}</td>   
                        <td>{{ $nota->nota_2 }}</td>   
                        <td>{{ $nota->nota_3 }}</td>    
                        @php $aux_nota = true; @endphp
                    @endif
                @endforeach
                @if (!$aux_nota)   
                    <td> - </td>   
                    <td> - </td>   
                    <td> - </td> 
                @endif
            </tr> 
        @endforeach
    </table>
    <br>
    <button type="button" id="salvar" class="btn btn-primary">Salvar dados</button>
@stop
@section('rodape')
@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function(){
        var URL_SITE = "{{ URL::to('/') }}";

        jQuery("#estudantes").click(function(){
            //var id = $("#estudantes").val();
            var id = $(this).val();

            window.location.href = URL_SITE + "/editaAluno/" + id;
        });
    });
</script>
@stop
