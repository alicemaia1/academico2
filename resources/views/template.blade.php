<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>
<body>
    <header>
        <h1> Acadêmico 2 </h1>
    </header>
    <main>
    <section class="menu">
        <h4>
            <a href="{{ route('principal')}}" > <span class="btn btn-outline-primary">Página Principal</span> </a>
            <a href="{{ route('listaAlunos')}}" > <span class="btn btn-outline-primary">Listagem de Estudantes</span> </a>
            <a href="{{ route('cadastraAlunos')}}" > <span class="btn btn-outline-primary">Cadastro de Estudantes</span> </a>
            <a href="{{ route('lancaNotas')}}" > <span class="btn btn-outline-primary">Lançamento de Notas</span> </a>
            <a href="{{ route('lancaFaltas')}}" > <span class="btn btn-outline-primary">Lançamento de Fatas</span> </a>
        </h4>
        
    </section>
    <section class="conteudo">
        <div class="container">
            @yield('conteudo')
        </div>
    </section>
</main>
<footer>
    <section class="rodape">
        @yield('rodape')
    </section>
    <div class="ml-4 text-center text-sm text-gray-500 sm:text-right sm:ml-0">
    </div>
    
</body>
</html>