@extends('template')
@section('conteudo')
    <h4> Lançamento de Faltas </h4>
    @csrf
    <table id="tbFaltas" class="table table-striped table-bordered">
        <tr>
            <th>Nome</th>
            <th>Turma</th>
            @foreach ($meses as $mes)
                <th>mês {{$mes->mes}}</th>
            @endforeach
        </tr>

        @foreach ($alunos as $aluno)
            <tr id="{{$aluno->id}}" class="faltas" >  
                <td><a href="{{ url('listaDados/'.$aluno->id)}}"> {{ $aluno->nome }} </a> </td>
                <td>{{ $aluno->turma }}</td>   
                @php $aux_falta = false; @endphp
                @foreach ($faltas as $falta)
                    @if ($aluno->id == $falta->id_aluno)
                        @foreach ($meses as $mes)
                            <td> <input type="number" name="falta_{{$mes->mes}}" value={{ $falta->faltas }} /> </td>   
                        @endforeach
                        @php $aux_falta = true; @endphp
                    @endif
                @endforeach
                @if (!$aux_falta)   
                    @foreach ($meses as $mes)
                        <td> <input type="number" name="falta_{{$mes->mes}}" value='' /> </td>   
                    @endforeach
                @endif
            </tr> 
        @endforeach
    </table>
    <br>
    <button type="button" id="salvar" class="btn btn-primary">Salvar dados</button>
@stop
@section('rodape')
@stop
@section('js')
<script type="text/javascript">

    jQuery(document).ready(function(){

        $("#salvar").click(function(){
            alert('salvar');

            var _token = $("input[name='_token']").val();
            //console.log('token: '+_token);

            var linhas = $("tr.faltas");
            linhas.each(function(){
                console.log($(this).attr('id'));
                console.log($(this).find('td').eq(0).text()); // nome
                console.log($(this).find('td').eq(1).text()); // turma
                console.log($(this).find('td input[name="falta_1"]').val());


                let id = $(this).attr('id');
                let nota_1 = $(this).find('td input[name="nota_1"]').val();
                let nota_2 = $(this).find('td input[name="nota_2"]').val();
                let nota_3 = $(this).find('td input[name="nota_3"]').val();

                $.ajax({
                    url: '{{ route("salvaNotas")}}',
                    method: 'POST',
                    data: { _token: _token,
                        id_aluno : id, 
                        nota_1 : nota_1, 
                        nota_2 : nota_2, 
                        nota_3 : nota_3},
                    dataType: 'json',
                    success: function (data){
                        console.log(data);
                        if(data.ok === true){
                            //window.location.href = "{{ route('cadastraAlunos')}}";
                            $('.msg').removeClass('d-none').addClass('alert-success').html(data.msg)
                        } else {
                            $('.msg').removeClass('d-none').addClass('alert-danger').html(data.msg);
                        }
                    }
                });
            });
        });
    });
</script>
@stop
